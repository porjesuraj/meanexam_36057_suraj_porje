const express = require('express')
const utils = require('../utils')
const db = require('../db')
const config = require('../config')
const jwt = require('jsonwebtoken')
const mailer = require('./../mailer')
const { request } = require('express')

const router = express.Router()


// ----------------------------------------------------
// GET
// ----------------------------------------------------
  
router.get('/employee-leave-status',(request,response) => {

    const statement = `select empId ,leaveId ,leaveDate ,leavePurpose ,leaveStatus  from apply_leave `
    db.query(statement,(error,data) => {
        response.send(utils.createResult(error,data))
    })
})

router.get('/employee-queries',(request,response) => {

    const statement = `select  qId,queryTitle,queryDesc,queryStatus,empId from emp_queries`
    db.query(statement,(error,data) => {
        response.send(utils.createResult(error,data))
    })
})

router.get('/meeting',(request,response) => {

    const statement = `select  meetingDate,meetingInfo,meetingStatus,adminId from meeting`
    db.query(statement,(error,data) => {
        response.send(utils.createResult(error,data))
    })
})

//  salId | noOfPresentDay | leaveCount | totalAmount | adminId | empId 
//(30 - Aprroved Leaves )*  800)
router.get('/generateSalarySlipForEmp/:Id',(request,response) => {
       const{Id} = request.params
    const statement = `select salId, noOfPresentDay,leaveCount,totalAmount,adminId,empId  from salary_slip where empId = ${Id} `
    db.query(statement,(error,data) => {
        response.send(utils.createResult(error,data))
    })
})
// ----------------------------------------------------



// ----------------------------------------------------
// POST
// ----------------------------------------------------
/* empId | empName | address | birth_date | gendor | email  | mobile     | password | role     | profilePhoto */


router.post('/signin', (request, response) => {
  const {email,password} = request.body
  const statement = `select empId,empName  from employee where email = '${email}' and password = '${password}' and role = 'admin' `
  db.query(statement, (error, users) => {
    if (error) {
      response.send({status: 'error', error: error})
    } else {
      if (users.length == 0) {
        response.send({status: 'error', error: 'user does not exist'})
      } else {
        const user = users[0]
        const token = jwt.sign({id: user['empId']}, config.secret)
        response.send(utils.createResult(error, {
            empName : user['empName'],
             token: token
        }))
      }
    }
  })
})


router.post('/signup', (request, response) => {
    const {empName,address,birth_date,gendor,email,mobile, password} = request.body
  
  const body = `
     <h1> welcome employee</h1>
     <h3> login email :  ${email} <h3>
     <h3> login password : ${password} <h3>      

  `
    const statement = `insert into employee (empName,address,birth_date,gendor,email,mobile, password,role)
     values ('${empName}', '${address}', '${birth_date}', '${gendor}','${email}','${mobile}','${password}','employee')`
    db.query(statement, (error, data) => {
  
      mailer.sendEmail(email,'Welcome employee',body,(error,info) => {
          console.log(error)
          console.log(info)
          response.send(utils.createResult(error,data))
      }  )
  
    })
  })
  

  router.post('/addNewMeetingDetails', (request, response) => {
    
    const {meetingDate,meetingInfo,meetingStatus} = request.body
    const statement = `insert into meeting(meetingDate,meetingInfo,meetingStatus,adminId) 
    values ('${meetingDate}', '${meetingInfo}', '${meetingStatus}', '${request.userId}')`


    db.query(statement, (error, data) => {
      if (error) {
        response.send({status: 'error', error: error})
      } else {
          response.send(utils.createSuccess(data))
       
      }
    })
  })
// ----------------------------------------------------


// ----------------------------------------------------
// PUT
// ----------------------------------------------------
router.put('/employee-leave-status',(request,response) => {
 
    const {status,empId,leaveId} = request.body
    const statement = ` update apply_leave set leaveStatus = '${status}' where empId = ${empId} and leaveId = ${leaveId}`
    db.query(statement,(error,data) => {
        response.send(utils.createResult(error,data))
    })
})

//meetingId | meetingDate         | meetingInfo        | meetingStatus | adminId 

router.put('/meting-status',(request,response) => {
 
    const {status,meetingId} = request.body
    const statement = ` update meeting set  meetingStatus  = '${status}' where adminId = ${request.userId} and meetingId = ${meetingId}`
    db.query(statement,(error,data) => {
        response.send(utils.createResult(error,data))
    })
})
// ----------------------------------------------------



// ----------------------------------------------------
// DELETE
// ----------------------------------------------------


// ----------------------------------------------------

module.exports = router
