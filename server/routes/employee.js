/* - signIn() 
- editProfile()
- applyLeave()
- viewLeavesStatus()
- meetingDetailsList()
- viewSalarySlip()
- addNewQueries()
- knowappliedQueryStatus() */

const express = require('express')
const utils = require('../utils')
const db = require('../db')
const config = require('../config')
const jwt = require('jsonwebtoken')
const mailer = require('./../mailer')
const { request } = require('express')

const router = express.Router()


// ----------------------------------------------------
// GET
// ----------------------------------------------------
  
router.get('/leave-status',(request,response) => {

    const statement = `select empId ,leaveId ,leaveDate ,leavePurpose ,leaveStatus  from apply_leave `
    db.query(statement,(error,data) => {
        response.send(utils.createResult(error,data))
    })
})



router.get('/meeting',(request,response) => {

    const statement = `select  meetingDate,meetingInfo,meetingStatus,adminId from meeting`
    db.query(statement,(error,data) => {
        response.send(utils.createResult(error,data))
    })
})

router.get('/SalarySlipForEmp',(request,response) => {
 
 const statement = `select salId, noOfPresentDay,leaveCount,totalAmount,adminId,empId  from salary_slip where empId = ${request.userId} `
 db.query(statement,(error,data) => {
     response.send(utils.createResult(error,data))
 })
})

router.get('/employee-queries',(request,response) => {

    const statement = `select  qId,queryTitle,queryDesc,queryStatus,empId from emp_queries where empId = ${request.userId}`
    db.query(statement,(error,data) => {
        response.send(utils.createResult(error,data))
    })
})
// ----------------------------------------------------



// ----------------------------------------------------
// POST
// ----------------------------------------------------
/* empId | empName | address | birth_date | gendor | email  | mobile     | password | role     | profilePhoto */


router.post('/signin', (request, response) => {
  const {email,password} = request.body
  const statement = `select empId,empName  from employee where email = '${email}' and password = '${password}' and role = 'employee' `
  db.query(statement, (error, users) => {
    if (error) {
      response.send({status: 'error', error: error})
    } else {
      if (users.length == 0) {
        response.send({status: 'error', error: 'user does not exist'})
      } else {
        const user = users[0]
        const token = jwt.sign({id: user['empId']}, config.secret)
        response.send(utils.createResult(error, {
            empName : user['empName'],
             token: token
        }))
      }
    }
  })
})

// leaveId | leaveDate  | leavePurpose | leaveStatus | empId 
router.post('/employee-leave',(request,response) => {
 
    const {leaveDate,leavePurpose} = request.body
    const statement = `insert into apply_leave (leaveDate,leavePurpose,empId)
    values('${leaveDate}','${leavePurpose}', '${request.userId}') `
    db.query(statement,(error,data) => {
        response.send(utils.createResult(error,data))
    })
})
// qId | queryTitle,queryDesc,queryStatus, empId 
router.post('/employee-query',(request,response) => {
 
    const {queryTitle,queryDesc} = request.body
    const statement = `insert into emp_queries (queryTitle,queryDesc,empId)
    values('${queryTitle}','${queryDesc}','${request.userId}') `
    db.query(statement,(error,data) => {
        response.send(utils.createResult(error,data))
    })
})

  

  
// ----------------------------------------------------


// ----------------------------------------------------
// PUT
// ----------------------------------------------------
router.put('/', (request, response) => {
    const {empName,address,birth_date,gendor,email,mobile, password} = request.body
  
  
    const statement = `update employee 
    set empName = '${empName}',
    address = '${address}',
    birth_date = '${birth_date}',
    gendor = '${gendor}',
    email = '${email}',
    mobile = '${mobile}',
     password = '${password}'
      where empId = '${request.userId}'`
    db.query(statement, (error, data) => {
  
     response.send(utils.createResult(error,data))
    })
  })


// ----------------------------------------------------



// ----------------------------------------------------
// DELETE
// ----------------------------------------------------


// ----------------------------------------------------

module.exports = router

